<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
        font-size: 14;
        
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
        
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #ffd52b;
        color: black;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: white;
    }

    .btnReg:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .btnReg:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .btnReg:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #ffcd04;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonPaging {
        background-color: white; 
        color: black; 
        border: 1px solid #4CAF50;
        border-radius: 4px;
        width: 30px;
        margin-top: 2px;
        margin-right: 1px;
        font-size: 12;
    }

    .buttonPagingCurrent{
        background-color: #4CAF50;
        border: 1px solid #4CAF50;
        border-radius: 4px;
        margin-right: 1px;
        margin-top: 2px;
        width: 30px;
        color: white;
        font-size: 12;
    }

    .button1:hover {
        background-color: #ffd52b;
        color: black;
    }
    .buttonPaging:hover {
        background-color: #4CAF50;
        color: white;
    }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
    }

    .nostyle-text{
        text-decoration: none; 
    }
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    a:active {
        text-decoration: none;
    }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 0; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }   

    
</style>

@if(Auth::user()->role != "Admin")
    <script>
        window.location ='{{ url("userProfile/".Auth::user()->id)}}';
    </script>
@endif

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div style="background-color: #16731a; color:white; font-size:20" class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in as ') }}
                    {{Auth::user()->role}}
                </div>
            </div>
        </div>
    </div>

    <br>
        
        <div id="containerUserList" class="container container75" style="border: black none 1px; min-height:80%">
            <div class="row justify-content-center" style="border:none red; width:; ">
                <div class="col-md-8" style="border:none cyan;height:80%;">
                    <div class="card" style="border:none yellow;height:80%;">
                        <div class="card-header" name="userList" style="background-color: #16731a; color:white; font-size:20"><b>User List</b></div>

                        <div class="card-body" style="text-align: right; overflow-x:auto; border:none purple; height:80%">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="width:100%">
                                <tr class="" style="background-color: white; border:none; ">
                                    <td style="border-top: none">
                                        Name
                                    </td>
                                    <td style="border-top: none">
                                        <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInput" onkeyup="myFunctionReset()" placeholder="Filter by name.." title="Type in a name">
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        Email
                                    </td>
                                    <td style="border-top: none">
                                        <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail" onkeyup="myFunctionReset()" placeholder="Filter by email.." title="Type in an email">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-top: none;">
                                        Phone no
                                    </td>
                                    <td style="border-top: none">
                                        <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputPhoneNo" onkeyup="myFunctionReset()" placeholder="Filter by phone no.." title="Type in a phone no">
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        &nbsp
                                    </td>
                                    <td style="border-top: none">
                                        Role
                                    </td>
                                    <td style="border-top: none">
                                        <select class="form-control" id="myInputRole" onchange="myFunctionReset()" title="Select a role">
                                            <option class="optionRole" selected>All</option>
                                            <option class="optionRole">Admin</option>
                                            <option class="optionRole">Store</option>
                                            <option class="optionRole">Student</option>
                                        </select>
                                        {{-- <input class="nostyle" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail" onkeyup="myFunction()" placeholder="Filter by email.." title="Type in an email"> --}}
                                    </td>
                                </tr>
                            </table>
                            <form name="StoreListForm" method="POST" action=""> 
                                {{-- {{ url('/sendemail') }} --}}
                            @csrf
                                <table style="border: none; width:100%" id="myTable">
                                    <tr>
                                        <td class="tdNoStyle" colspan="2">
                                            <button type="button" id="addUserButton" value="Add User" class="button1" style="width: auto; margin-bottom:2; " onclick="editModalShow()">Add User</button> 
                                            {{--  --}}
                                        </td>
                                        <td colspan="3" class="tdNoStyle" style="font-size:12; text-align:right; vertical-align:bottom">
                                            <span>
                                                Show entries : 
                                                <select class="" id="inputShowEntries" onchange="myFunctionReset()" style="margin-bottom: 2; margin-right:">
                                                    <option selected>10</option>
                                                    <option>20</option>
                                                    <option>30</option>
                                                    <option>100</option>
                                                </select>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdNoStyle" >&nbsp</td>
                                    </tr>
                                    <tr class="trBorderLeftRight">
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Phone No.
                                        </th>
                                        <th>
                                            Role
                                        </th>
                                        {{-- header select to bulk remove <th style="text-align: center;">
                                            Select
                                        </th> --}}
                                        <th colspan = 2>
                                            Actions
                                        </th>
                                    </tr>
                                    @if($users == null)
                                        <tr>
                                            <td colspan="5" style="text-align: center;">
                                                No users registered yet!
                                            </td>
                                        </tr>
                                    @else
                                        @foreach($users as $user)
                                            <tr class="trBorderLeftRight">
                                                <td class="tdNo">
                                                    {{ __($user['id'])}}
                                                </td>
                                                <td class="tdName">
                                                    @if($user['role']=="Store")
                                                        @if($user->store != null)
                                                            {{ __($user->store->storeName)}}
                                                        @else
                                                            {{ __($user['name'])}}
                                                        @endif
                                                    @else
                                                        {{ __($user['name'])}}
                                                        {{-- <a style="text-decoration: none; color:black" href=""></a> --}}
                                                        {{-- {{url('userProfile/'.$user['id'])}} --}}
                                                    @endif
                                                </td>
                                                <td class="tdEmail">
                                                    {{ __($user['email'])}}
                                                </td>
                                                <td class="tdPhoneNo" style="text-align: left;">
                                                    {{ __($user['phone_number'])}}
                                                    {{-- tick button to bulk remove @if($user['userRole']=="Admin")
                                                        <input type="checkbox" name="id[]" value="{{ __($user['id'])}}" onchange="EnableSubmit(this)" disabled>
                                                    @else
                                                        <input type="checkbox" name="id[]" value="{{ __($user['id'])}}" onchange="EnableSubmit(this)" id="def">
                                                    @endif --}}
                                                </td>
                                                <td class="tdRole">
                                                    {{ __($user['role'])}}
                                                </td>
                                                <td colspan = 2>
                                                    @if($user->role != 'Admin')

                                                    <a href="{{url('userProfile/'.$user['id'])}}" class="btn btn-primary">View</a>
                                                    @endif
                                                    &nbsp
                                                    {{-- <input type="submit"  class="btn btn-danger" value="Delete" method= "post" formaction="{{ url('/deleteUser') }}" onclick="return confirm('Confirm delete?')" ></span> --}}
                                                </td>
                                                {{-- <td>
                                                    <form action="{{ url('/deleteUser') }}" method="POST"> 
                                                        {{-- {{ route('', $user['id'])}} 
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
                                                    </form>
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                        <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                            <td colspan="5" style="text-align:center">
                                                No user matched you filter...
                                            </td>
                                        </tr>  
                                    @endif
                                </table>
                                {{-- <span><input type="submit" value="Submit" style="background-color: #039c36; color:white; border-radius: 8px; border: none; padding: 5px 15px;"></span> --}}
                                <div id="pagingDiv" style="border:none;">
                                    <span id="pagingSpan" style="font-size:"></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


{{-- POP UP BLOCK --}}

<br>
<div id="editModal" class="modal" style="border: none;">
    <div class="row justify-content-center" id="editModal2" >
        <div class="col-md-8 row justify-content-center" id="editModal3" style="border: none; text-align:center" >
            <div class="card" id="editCard" style="border: none; width: 600">
                <div class="card-header" style="background-color: #16731a; color:white"><b>{{ __('Add new user') }}</b></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('adminRegister') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                
                
                        <div class="form-group row">
                            <label for="Phone Number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                            <div class="col-md-6">
                                <input type="tel" id="phone" name="userPhone" class="form-control" placeholder="Example : 0121234567" pattern="[0-9]{10}" value="{{ old('userPhone') }}" required autocomplete="userPhone">
                            </div>
                        </div>
                
                        {{-- <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>
                            <div class="col-md-6">
                                <select name="userGender" class="form-control" required>
                                    <option disabled {{ old('userGender') == "" ? 'selected' : '' }} hidden value="">Select gender..</option>
                                    <option value="Male" {{ old('userGender') == "Male" ? 'selected' : '' }}>Male</option>
                                    <option value="Female" {{ old('userGender') == "Female" ? 'selected' : '' }}>Female</option>
                                </select>
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="userRole" class="col-md-4 col-form-label text-md-right">{{ __('Role ') }}</label>
                            <div class="col-md-6">
                                <select name="userRole" class="form-control" required>
                                    <option disable {{ old('userRole') == "" ? 'selected' : '' }} hidden value="">Select role..</option>
                                    <option {{ old('userRole') == "Admin" ? 'selected' : '' }} value="Admin">Admin</option>
                                    <option {{ old('userRole') == "Store" ? 'selected' : '' }} value="Store">Store</option>
                                    <option {{ old('userRole') == "Student" ? 'selected' : '' }} value="Student">Student</option>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="adminReg" value="1">

                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                            {{-- <div>     --}}
                                <button type="submit" class="btnReg" style="width: 100">
                                    {{ __('Submit') }}
                                </button>
                                &nbsp &nbsp
                                <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                    {{ __('Cancel') }}
                                </button>
                            {{-- <div class="col-md-6 offset-md-4" style="text-align: center; border:solid"> --}}
                            {{-- </div> --}}
                        </div>
                
                        {{-- <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btnReg">
                                    {{ __('Register') }}
                                </button>
                                <button type="Reset" class="buttonReject">
                                    {{ __('Cancel') }}
                                </button>
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




@if(session()->get('addedUser') != null)
    <span id="addedUser" style="display: none">{{session()->get('addedUser')}}</span>
@endif


<script>
    
    window.onload = function(){ //run these after page loaded
        myFunction();
        myFunction2();
        selectFunction();

        if(document.getElementsByClassName('invalid-feedback')[0]!= null)
        {
            var editModal = document.getElementById("editModal");
            var addUserButton = document.getElementById("addUserButton");
            // editModal.style.display = "block";
            addUserButton.scrollIntoView();
            editModalShow();                    
        }
        if(document.getElementById('deletedUser') != null){
            setTimeout(function userDeletedAlert(){
                alert("Successfully Deleted : " + document.getElementById('deletedUser').innerHTML);
            },500);
        }
        if(document.getElementById('addedUser') != null){
            setTimeout(function userRegisteredAlert(){
                alert("Successfully Added : " + document.getElementById('addedUser').innerHTML);
            },500);
        }
        if(document.getElementById('approvedUsersCounter') != null){
            setTimeout(function usersApprovedAlert(){
                alert("Successfully Approved " + document.getElementById('approvedUsersCounter').innerHTML + " user(s)");
            },500);
        }
        if(document.getElementById('rejectedUsersCounter') != null){
            setTimeout(function usersRejectedAlert(){
                alert("Successfully Rejected " + document.getElementById('rejectedUsersCounter').innerHTML + " user(s)");
            },500);
        }
    };
    
    EnableSubmit = function(val)
    {
        var counter = 0;
        var counter1 = 0;
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");

        var superCheckBox = document.getElementById('superCheckBox');
        var checkBoxSelect = document.getElementsByClassName('checkBoxSelect');

        for(i = 0; i < checkBoxSelect.length; i++){
            if(checkBoxSelect[i].parentElement.parentElement.style.display=="" && checkBoxSelect[i].checked == true){
                counter++;
            }
        }
        if(counter>0){
            sbmt.disabled = false;
            sbmt1.disabled = false;
        }else{
            sbmt.disabled = true;
            sbmt1.disabled = true;
            superCheckBox.checked = false;
        }
        // alert(counter);
    };

    
    EnableSubmit2 = function(val2)
    {
        var counter2 = 0;
        var counter3 = 0;
        var sbmt2 = document.getElementById("mno");
        
        if (val2.checked == true)
        {   
            counter2 = counter2 + 1;
        }
        else
        {
            counter2 = counter2 - 1; 
        }

        if (counter2 > 0)
        {   
            sbmt2.disabled = false;
        }
        else
        {
            sbmt2.disabled = true;
        }

        var sbmt3 = document.getElementById("pqr");
        
        if (val2.checked == true)
        {   
            counter3 = counter3 + 1;
        }
        else
        {
            counter3 = counter3 - 1; 
        }

        if (counter3 > 0)
        {   
            sbmt3.disabled = false;
        }
        else
        {
            sbmt3.disabled = true;
        }

    };

    

    // *********** user list filter + paging script starts here ***************
    function selectFunction(){
        var inputRole1 = document.getElementById("myInputRole");
        var optionRole = inputRole1.getElementsByTagName("option");
        for(var i = 0; i<optionRole.length; i++){
            // alert("option index : " + i + " | inputRoleValue : " + inputRole1.value);
            if(inputRole1.value == optionRole[i].value){
                optionRole[i].hidden = true;
            }else{
                optionRole[i].hidden = false;
            }

        }
    };

    var currentPage = 1; //default current page
    var showEntries = 10; //default show entries
    var checkRegSuccess = 0;

    function myFunctionReset(){
        currentPage = 1; //reset current page position
        showEntries = document.getElementById("inputShowEntries").value;
        
        selectFunction();
        myFunction();
        var pagingSpan = document.getElementById('pagingSpan');
        if(showEntries <= 20){
            pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
        }
    };
    
    function myFunction() {
        var input, filter, table, tr, td, tdState, tdNo, tdRole, tdPhoneNo, i, txtValue, txtValueState, inputState, filterState, inputRole, txtValueRole, inputPhoneNo, filterPhoneNo, txtValuePhoneNo, pagingSpan, counterTr = 0, counterPageButton = 0, oldPagingButton = 0;
        input = document.getElementById("myInput");
        inputState = document.getElementById("myInputEmail");
        inputRole = document.getElementById("myInputRole");
        inputPhoneNo = document.getElementById("myInputPhoneNo");
        filter = input.value.toUpperCase();
        filterState = inputState.value.toUpperCase();
        filterRole = inputRole.value.toUpperCase();
        filterPhoneNo = inputPhoneNo.value;
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            tdNo = tr[i].getElementsByClassName("tdNo")[0];
            td = tr[i].getElementsByClassName("tdName")[0];
            tdState = tr[i].getElementsByClassName("tdEmail")[0];
            tdRole = tr[i].getElementsByClassName("tdRole")[0];
            tdPhoneNo = tr[i].getElementsByClassName("tdPhoneNo")[0];
            if (td && tdState && tdRole && tdPhoneNo) {
                txtValue = td.textContent || td.innerText;
                txtValueState = tdState.textContent || tdState.innerText;
                txtValueRole = tdRole.textContent || tdRole.innerText;
                txtValuePhoneNo = tdPhoneNo.textContent || tdPhoneNo.innerText;
                if(inputRole.value.toUpperCase() == "ALL"){
                    if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                        if((counterTr >= (currentPage-1)*showEntries) && ((counterTr+1) <= (currentPage*showEntries))){
                            tr[i].style.display = "";
                            tdNo.innerHTML=counterTr+1;
                            // alert("name : " + filter + "| email : " + filterState);
                        }else{
                            tr[i].style.display = "none";
                        }
                        counterTr++;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
                else{
                    if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValueRole.toUpperCase().indexOf(filterRole) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                        if((counterTr >= (currentPage-1)*showEntries) && ((counterTr+1) <= (currentPage*showEntries))){
                            tr[i].style.display = "";
                            tdNo.innerHTML=counterTr+1;
                            // alert("tr number : " + counterTr);
                        }else{
                            tr[i].style.display = "none";
                        }
                        counterTr++;
                    }else{
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        pagingSpan = document.getElementById("pagingSpan");
        pagingSpan.innerHTML = "";

        var trNoResult = document.getElementById('trNoResult');

        if((document.getElementById('regSuccess') != null) && (checkRegSuccess == 0)){
                currentPage = Math.ceil(counterTr/showEntries);
                checkRegSuccess++;
                myFunction();
                pagingSpan.scrollIntoView();
        }

        if(counterTr == 0){
            trNoResult.style.display = "";
        }else{
            trNoResult.style.display = "none";
        }

        if(counterTr/showEntries > 1){
            pagingSpan.innerHTML = "Page : ";
            for(i = 1; i <= Math.ceil(counterTr/showEntries); i++){
                var pagingButton = document.createElement('button');
                if(i == currentPage){
                    pagingButton.className = "buttonPagingCurrent";
                    pagingButton.disabled = true;
                }
                else{
                    pagingButton.className = "buttonPaging";
                }
                pagingButton.type = "button";
                pagingButton.addEventListener('click', pageNumberClicked);
                pagingButton.value = i;
                pagingButton.innerHTML = i;
                pagingSpan.appendChild(pagingButton); 
                counterPageButton++;
            }
            
        }
    };

    function pageNumberClicked(e){
        currentPage = e.target.value;
        myFunction();
        var pagingSpan = document.getElementById('pagingSpan');
        var addUserButton = document.getElementById('addUserButton');
        if(showEntries <= 20){
            pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
        }else{
            addUserButton.scrollIntoView();
        }
    };

    // ************ user list filter + paging ends here****************

    var currentPage2 = 1; //default current page
    var showEntries2 = 10; //default show entries
    var checkRegSuccess2 = 0;

    function myFunctionReset2(){
        currentPage2 = 1; //reset current page position
        oldShowEntries2 = showEntries2;
        showEntries2 = document.getElementById("inputShowEntries2").value;
        
        myFunction2();
        deselectAll();

        var myInput2 = document.getElementById('myInput2');
        if(showEntries2 <= 20 && oldShowEntries2!=showEntries2){
            myInput2.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
        }
    };
    
    function myFunction2() {
        // var input, filter, table, tr, td, tdState, tdNo, tdRole, tdPhoneNo, i, txtValue, txtValueState, inputState, filterState, inputRole, txtValueRole, inputPhoneNo, filterPhoneNo, txtValuePhoneNo, pagingSpan, counterTr = 0, counterPageButton = 0, oldPagingButton = 0;
        // input = document.getElementById("myInput2");
        // inputState = document.getElementById("myInputEmail2");
        // // inputRole = document.getElementById("myInputRole2");
        // inputPhoneNo = document.getElementById("myInputPhoneNo2");
        // filter = input.value.toUpperCase();
        // filterState = inputState.value.toUpperCase();
        // // filterRole = inputRole.value.toUpperCase();
        // filterPhoneNo = inputPhoneNo.value;
        // table = document.getElementById("myTable2");
        // tr = table.getElementsByTagName("tr");
        // for (i = 0; i < tr.length; i++) {
        //     tdNo = tr[i].getElementsByClassName("tdNo")[0];
        //     td = tr[i].getElementsByClassName("tdName")[0];
        //     tdState = tr[i].getElementsByClassName("tdEmail")[0];
        //     tdRole = tr[i].getElementsByClassName("tdRole")[0];
        //     tdPhoneNo = tr[i].getElementsByClassName("tdPhoneNo")[0];
        //     if (td && tdState && tdRole && tdPhoneNo) {
        //         txtValue = td.textContent || td.innerText;
        //         txtValueState = tdState.textContent || tdState.innerText;
        //         txtValueRole = tdRole.textContent || tdRole.innerText;
        //         txtValuePhoneNo = tdPhoneNo.textContent || tdPhoneNo.innerText;
        //         if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
        //             if((counterTr >= (currentPage2-1)*showEntries2) && ((counterTr+1) <= (currentPage2*showEntries2))){
        //                 tr[i].style.display = "";
        //                 tdNo.innerHTML=counterTr+1;
        //                 // alert("tr number : " + counterTr);
        //             }else{
        //                 tr[i].style.display = "none";
        //             }
        //             counterTr++;
        //         }else{
        //             tr[i].style.display = "none";
        //         }
        //     }
        // }

        // pagingSpan = document.getElementById("pagingSpan2");
        // pagingSpan.innerHTML = "";

        // var trNoResult = document.getElementById('trNoResult2');

        // if(counterTr == 0){
        //     trNoResult.style.display = "";
        // }else{
        //     trNoResult.style.display = "none";
        // }

        // if(counterTr/showEntries2 > 1){
        //     pagingSpan.innerHTML = "Page : ";
        //     for(i = 1; i <= Math.ceil(counterTr/showEntries2); i++){
        //         var pagingButton = document.createElement('button');
        //         if(i == currentPage2){
        //             pagingButton.className = "buttonPagingCurrent";
        //             pagingButton.disabled = true;
        //         }
        //         else{
        //             pagingButton.className = "buttonPaging";
        //         }
        //         pagingButton.type = "button";
        //         pagingButton.addEventListener('click', pageNumberClicked2);
        //         pagingButton.value = i;
        //         pagingButton.innerHTML = i;
        //         pagingSpan.appendChild(pagingButton); 
        //         counterPageButton++;
        //     }
            
        // }
    };

    function pageNumberClicked2(e){
        currentPage2 = e.target.value;
        myFunction2();
        deselectAll();
        // var myInput2 = document.getElementById("myInput2");
        // myInput2.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling

        var myInput2 = document.getElementById('myInput2');
        if(showEntries2 >= 20){
            myInput2.scrollIntoView(); // scroll the page make sure user can see filter text fields
        }

        // var pagingSpan = document.getElementById('pagingSpan2');
        // var addUserButton = document.getElementById('addUserButton2');
        // if(showEntries <= 20){
        //     // pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
        // }else{
        //     // addUserButton.scrollIntoView();
        // }
    };


    function selectAll(){
        var box = document.getElementsByClassName("checkBoxSelect");
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");
        if(document.getElementById("superCheckBox").checked == true){
            for(i = 0; i<box.length; i++ ){
                // box[i].checked = true;

                if(box[i].parentElement.parentElement.style.display==""){
                    box[i].checked = true;
                }
                else{
                    box[i].checked = false;
                }   
            }
            sbmt.disabled = false;
            sbmt1.disabled = false;
        }else{
            deselectAll();
        }
    }

    function deselectAll(){
        var superCheckBox = document.getElementById("superCheckBox");
        superCheckBox.checked = false;
        var box = document.getElementsByClassName("checkBoxSelect");
        for(i = 0; i<box.length; i++ ){
            box[i].checked = false;
        }
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");
        sbmt.disabled = true;
        sbmt1.disabled = true;
    }

    var editModal = document.getElementById("editModal");
    var disabledButton = document.getElementById("buttonDisabled");
    var editCard = document.getElementById("editCard");

    function eventCloseEditFunction() {
        const body = document.body;
        // const scrollY = body.style.top;
        // body.style.position = '';
        // body.style.top = '';
        // body.style.height = '';
        body.style.overflowY = '';
        // window.scrollTo(0, parseInt(scrollY || '0') * -1);
        editModal.style.display = "none";
    };

    function editModalShow() {
        editModal.style.display = "block";
        editModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
    };

    document.addEventListener("click", function(e)
    {
        // var container = $("editModal");
        // alert(e.target);

        // if the target of the click isn't the container nor a descendant of the container
        // if (!editCard.is(e.target))

        if ((e.target==editModal || e.target==editModal2 || e.target==editModal3)) 
        {
            eventCloseEditFunction();
            // editModal.hide();
            
        }
        // if ((e.target==disabledButton)) 
        // {
        //     // editModal.hide();
        //     alert("Hello! I am an alert box!!");
        // }

    });

</script>
    


@endsection
