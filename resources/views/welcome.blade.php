<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>TCoCoS</title>
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #16731a; color:white;">
            <div class="container px-4" >
                    <a class="navbar-brand" href="">TCoCoS</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ms-auto">
                            <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                            <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                            <li class="nav-item"><a class="nav-link" href="#contact">Contact Us</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        </ul>
                    </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="text-white" style="background-color: #16731a;">
            <div class="container px-4 text-center">
                <h1 class="fw-bolder">TCoCoS</h1>
                <p class="lead">TAZU Coupon Collection System</p>
                {{-- <a class="btn btn-lg btn-light" href="#about">Start scrolling!</a> --}}
            </div>
        </header>
        <!-- About section-->
        <section id="about">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>About this website</h2>
                        &nbsp
                        <p class="lead">TAZU Coupon Collection System (TCoCoS) is web-based application for TAZU ‘Kupon Makan @ Go Kitchen’ programme.</p>
                        <p class="lead">TAZU Coupon Collection System (TCoCoS) generates E-coupons for students who are sponsored under TAZU and stores who are in the ‘Kupon Makan @ Go Kitchen’ programme can add coupons and keep track of the coupons claimed for reimbursement. </p>
                        
                        {{-- <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
                        <ul>
                            <li>Clickable nav links that smooth scroll to page sections</li>
                            <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                            <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
                            <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                        </ul> --}}

                    </div>
                </div>
            </div>
        </section>
        <!-- Services section-->
        <section class="bg-light" id="services">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>Services we offer</h2>
                        &nbsp
                        <p class="lead">Tabung Amanah Zakat UNITEN implements the distribution of zakat based on the agreement between Universiti Tenaga Nasional and Lembaga Zakat Selangor, ie Universiti Tenaga Nasional is only allowed to distribute zakat to only four asnafs out of the eight asnafs, namely:-</p>
                        <ul>
                            <li>Fakir</li>
                            <li>Miskin (Poor)</li>
                            <li>Muallaf</li>
                            <li>Fisabilillah</li>
                        </ul>
                        &nbsp
                        <p>Further information is detailed in the link below:</p><a href="https://amanahuniten.my/agihanPage.php" target="_blank">https://amanahuniten.my/agihanPage.php</a>

                    </div>
                </div>
            </div>
        </section>
        <!-- Contact section-->
        <section id="contact">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>Contact us</h2>
                        <address>
                            <strong>Tabung Amanah Zakat UNITEN (TAZU)</strong>
                            <br>Universiti Tenaga Nasional
                            <br>
                          </address>
                          <address>
                            <abbr title="Phone">P:</abbr>
                            03-89212030
                            <br>
                            <abbr title="Fax">F:</abbr>
                            03-89287113
                            <br>
                            <abbr title="Email">E:</abbr>
                            <a href="mailto:#">zakatuniten@gmail.com </a>
                        </address>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="py-5 " style="background-color: #16731a; color:white;">
            <div class="container px-4"><p class="m-0 text-center text-white">Copyright &copy; FYP Project: TCoCoS</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
