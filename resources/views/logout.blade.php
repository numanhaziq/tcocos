@guest
please login
@else
    
    {{ Session::flush() }}
    <script>
        window.location = "/";
    </script>
    
@endguest