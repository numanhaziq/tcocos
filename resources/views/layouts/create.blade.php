@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
       <h1 class="display-3">Add a User</h1>
     <div>
       @if ($errors->any())
         <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
               @endforeach
           </ul>
         </div><br />
       @endif
         <form method="post" action="{{ route('users.store') }}">
             @csrf
             <div class="form-group">    
                 <label for="name">Name:</label>
                 <input type="text" class="form-control" name="first_name"/>
             </div>
   
             <div class="form-group">
                 <label for="email">Email:</label>
                 <input type="text" class="form-control" name="email"/>
             </div>

             <div class="form-group">
                 <label for="phone_number">Phone Number:</label>
                 <input type="text" class="form-control" name="phone_number"/>
             </div>

             <div class="form-group">
                 <label for="role">Role:</label>
                 <input type="text" class="form-control" name="role"/>
             </div>                         
             <button type="submit" class="btn btn-primary-outline">Add user</button>
         </form>
     </div>
   </div>
</div>
   
@endsection