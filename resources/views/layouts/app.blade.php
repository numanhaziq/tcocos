<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <style>
    
        .btnReg:enabled {
            background-color: white; 
            color: black; 
            border: 2px solid #4CAF50;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .btnReg:hover {
            background-color: #4CAF50;
            color: white;
            }
        
        .btnReg:disabled{
            background-color: #ffffff;
            color: grey; 
            border: 2px solid grey;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .button3:enabled {
            background-color: white; 
            color: black; 
            border: 2px solid #4CAF50;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .buttonPaging {
            background-color: white; 
            color: black; 
            border: 1px solid #4CAF50;
            border-radius: 4px;
            width: 30px;
            margin-top: 2px;
            margin-right: 1px;
            font-size: 12;
        }
    
        .buttonPagingCurrent{
            background-color: #4CAF50;
            border: 1px solid #4CAF50;
            border-radius: 4px;
            margin-right: 1px;
            margin-top: 2px;
            width: 30px;
            color: white;
            font-size: 12;
        }
    
        .button3:hover {
            background-color: #4CAF50;
            color: white;
        }
        .buttonPaging:hover {
            background-color: #4CAF50;
            color: white;
        }
        
        .button1:disabled{
            background-color: #ffffff;
            color: grey; 
            border: 2px solid grey;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .buttonReject:enabled {
            background-color: white; 
            color: black; 
            border: 2px solid #a10c25;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .buttonReject:hover {
            background-color: #a10c25;
            color: white;
            }
        
        .buttonReject:disabled{
            background-color: #ffffff;
            color: grey; 
            border: 2px solid grey;
            border-radius: 4px;
            width: 75px;
            margin-top: 5px;
            margin-right: 0px
        }
    
        .nostyle{
            -webkit-appearance: none; 
            border:1px solid;
        }
    
        .nostyle-text{
            text-decoration: none; 
        }
        a:link {
            text-decoration: none;
        }
        a:visited {
            text-decoration: none;
        }
    
        a:hover {
            text-decoration: none;
        }
    
        a:active {
            text-decoration: none;
        }
    
        .modal {
            display: block; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 0; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            /* background-color: #fefefe; */
            background-color: #3D3D3D;
            text-align: center;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            /* width: 10%; */
        }   
    
        
    </style>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('TCoCoS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav style="background-color:#16731a" class="navbar navbar-expand-md navbar-light bg-green shadow-sm"> {{-- style="background-color:#16731a" --}}
            <div class="container">
                @if(Auth::user() != null )
                <a class="navbar-brand" href="{{ url('/home') }}" style="color:white"> {{--style="color:white"--}}
                @else
                <a class="navbar-brand" href="{{ url('/') }} " style="color:white">
                @endif
                    TCoCoS
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a style="color:white" class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            {{-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif --}}
                        @else
                            <li class="nav-item dropdown">
                                <a  style="color:white" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    @if(Auth::user() != null )
                                        {{-- <a class="dropdown-item" href="{{ url('/home') }}">
                                        <!-- <a class="dropdown-item" href="{{ url('/userlist') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('Store List') }}
                                        </a> --}}
                                        {{-- <a class="dropdown-item" href="{{ url('/classManagement') }}">
                                            <!-- <a class="dropdown-item" href="{{ url('/userlist') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('Class Management') }}
                                        </a>
                                        <a class="dropdown-item" href="{{ url('/userManagement') }}">
                                            <!-- <a class="dropdown-item" href="{{ url('/userlist') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('Credit Management') }}
                                        </a> --}}
                                    
                                        <a class="dropdown-item" href="{{ url('/home') }}">
                                            {{ __('Home') }}
                                        </a>
                                        @if(Auth::user()->role != "Admin")
                                        <a class="dropdown-item" href="{{url('userProfile/'.Auth::user()->id)}}">
                                            {{ __('My Profile') }}
                                        </a>
                                        @endif
                                        <a class="dropdown-item" onclick="changePasswordModalShow()">
                                            {{-- href="{{url('userProfile/'.Auth::user()->id)}}" --}}
                                            {{ __('Change Password') }}
                                        </a>
                                    @endif
                                    {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="return confirm('Sure logout?');event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a> --}}
                                    <a class="dropdown-item" href="{{ url('/index/logout') }}"
                                       onclick="return confirm('Sure logout?')">
                                        {{ __('Logout') }}
                                    </a>

                                    {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a> --}}

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    {{-- POP UP BLOCK --}}

    <div class="modal" id="changePasswordModal" style="border: solid">
        <div class="row justify-content-center" id="changePasswordModal2" >
            <div class="col-md-8 row justify-content-center" id="changePasswordModal3" style="border: none; text-align:center" >
                <div class="card" id="editCard" style="border: none; width: 60%">
                    <div class="card-header" style="background-color: #16731a; color:white"><b>{{ __('Change Password') }}</b></div>
                    <div class="card-body">
                        @if(Auth::user() != null)
                        <form method="POST" action="{{ url('/changePassword') }}" >
                            @csrf
                            <input type="hidden" name="userID" value="{{ Auth::user()->id}}">
                            <div class="form-group row">
                                <label for="currentPassword" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>
                                <div class="col-md-6">
                                    <input id="currentPassword" type="password" class="form-control @error('currentPassword') is-invalid @enderror" name="currentPassword" required autofocus>
                                    @error('currentPassword')
                                        <span class="invalid-feedback-password" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="newPassword" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>
                                <div class="col-md-6">
                                    <input id="newPassword" type="password" class="form-control @error('newPassword') is-invalid @enderror" name="newPassword" required autofocus>
                                    @error('newPassword')
                                        <span class="invalid-feedback-password" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="confirm_newPassword" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>
                                <div class="col-md-6">
                                    <input id="confirm_newPassword" type="password" class="form-control @error('confirm_newPassword') is-invalid @enderror" name="confirm_newPassword" required autofocus>
                                    @error('confirm_newPassword')
                                        <span class="invalid-feedback-password" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                <button type="submit" class="button3" style="width: 100">
                                    {{ __('Submit') }}
                                </button>&nbsp &nbsp
                                <button type="reset" class="buttonReject" style="width: 100" onclick="eventClosePasswordFunction()">
                                    {{ __('Cancel') }}
                                </button>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session()->get('changePasswordSuccess') != null)
            <span id="changePasswordSuccess" style="display: none">{{ session()->get('changePasswordSuccess') }}</span>
            <script>
                alert(document.getElementById("changePasswordSuccess").innerText);
            </script>
    @endif

    <script>
        
    

        var changePasswordModal = document.getElementById("changePasswordModal");
        var changePasswordModal2 = document.getElementById("changePasswordModal2");
        var changePasswordModal3 = document.getElementById("changePasswordModal3");


        function changePasswordModalShow() {
            changePasswordModal.style.display = "block";
            changePasswordModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        document.addEventListener('keydown', function(event){
            if(event.key === "Escape")
            {
                eventClosePasswordFunction();
            }
        });

        document.addEventListener("click", function(e)
        {
            if ( e.target==changePasswordModal || e.target==changePasswordModal2 || e.target==changePasswordModal3 ) 
            {
                eventClosePasswordFunction();
            }
        });

        function eventClosePasswordFunction() {
            const body = document.body;
            body.style.overflowY = '';
            changePasswordModal.style.display = "none";

        };
    
        
    </script>

</body>
</html>
