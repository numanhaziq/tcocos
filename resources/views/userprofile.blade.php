<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #ffd52b;
        color: black;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50;
        color: white;
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }

    .button1:disabled{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25;
        color: white;
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white;
        color: black;
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }

    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none;
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #16731a;
    }
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header" style="background-color: #16731a; color: white;font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                    <table style="border:none; margin-bottom:0; padding:0;">
                        <tr>
                            <th class="tdNoStyle" style="font-size:20; vertical-align:middle; color: white;">
                                <b>{{$firstName[0]}}</b>
                            </th>
                            <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle">
                                @if(Auth::user()->id == $user['id'] || Auth::user()->role == "Admin")
                                    <button type="button" class="btn btn-primary" style ="height:; padding:2; margin-right: 2"onclick="editModalShow()" >
                                        {{-- <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user" alt="Delete icon"/> --}}
                                        Edit
                                    </button>

                                @endif
                                @if(Auth::user()->role == "Admin" )
                                    <button form="deleteForm" class="btn btn-danger" style ="height:; padding:2; margin-right: 2" onclick="return confirm('Confirm delete?')" >
                                        {{-- <img class="deletebutton2" src="/images/icons/deletered.png" style="width: 25" title="Delete user" alt="Delete icon"/> --}}
                                        Delete
                                    </button>
                                    <form name="deleteForm" id="deleteForm"  method="POST" action="{{ url('/deleteUser') }}">
                                        @csrf
                                        <input type="hidden" name="userID" value="{{$user['id']}}">
                                    </form>
                                @endif
                            </th>
                        </tr>
                    </table>
                </div>

                <div class="card-body" style="text-align: right">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form name="ApprovalForm" method="POST" action="">
                        {{-- {{ url('/deleteUser') }} --}}
                        @csrf
                        <table style="border-collapse: collapse;">
                            <tr>
                                @php
                                    $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                @endphp
                                @if(file_exists($ppLink))
                                    <td style="text-align: center; border-top:none">
                                        <img id="ppLink" src="/public/{{$ppLink}}" height="250" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                    </td>
                                @endif
                            </tr>
                        </table>
                        <table style="border: 1px solid black; width:100%;">
                            <tr style="font-weight: bold">
                                <th style="width: 5%; font-weight: bold; border:1px solid none">
                                    ID
                                </th>
                                <th colspan="" style="font-weight: bold; border:1px solid none">
                                    Name
                                </th>
                                <th colspan="" style="font-weight: bold; border:1px solid none">
                                    Email
                                </th>
                                <th colspan="3" style="font-weight: bold; border:1px solid none">
                                    Phone Number
                                </th>
                            </tr>
                            <tr>
                                <td style="font-weight: ; border:1px solid none">
                                    <span id='userID'>{{ __($user['id'])}}</span>
                                </td>
                                <td colspan="" style="width:30%; font-weight: ; border:1px solid none">
                                    {{ __($user['name'])}}
                                </td>
                                <td colspan="" style="font-weight: ; border:1px solid none">
                                    {{ __($user['email'])}}
                                </td>
                                <td colspan="3" style="border:1px solid none">
                                    {{ __($user['phone_number'])}}
                                </td>
                            </tr>

                            @if($user['role'] != "Student")
                            <tr>
                                <th colspan="2" style="font-weight: bold; border:1px solid none;">
                                    Store Name
                                </th>
                                <th colspan="2" style="font-weight: bold; border:1px solid none">
                                    Store Address
                                </th>
                                <th colspan="2" style="font-weight: bold; border:1px solid none">
                                    Role
                                </th>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="2" style=" border:1px solid none">
                                    @if($user->store == null) 
                                    <i>Store Name</i>
                                    @else
                                    {{__($user->store->storeName)}} 
                                    @endif
                                </td>
                                <td colspan="2" style=" border:1px solid none">
                                    @if($user->store == null) 
                                    <i>Store Address</i>
                                     @else 
                                    {{__($user->store->storeAddress)}} 
                                     @endif
                                     
                                </td>
                                <td colspan="2" style=" border:1px solid none">
                                    {{ __($user['role'])}}
                                </td>
                            </tr>

                            @else
                            
                            <tr>
                                <th colspan="2" style="font-weight: bold; border:1px solid none">
                                </th>
                                <th colspan="4" style="font-weight: bold; border:1px solid none">
                                    Role
                                </th>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-weight: bold; border:1px solid none">
                                </td>
                                <td colspan="4" style=" border:1px solid none">
                                    {{ __($user['role'])}}
                                </td>
                            </tr>
                            @endif

                        </table>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>

<br>
@if($user['role'] == "Store")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    
                    <div class="card-header" style="background-color: #16731a; color:white; font-size:20"><b>Pending Coupons</b>
                        <button type="button" class="btn btn-primary" style ="height:; padding:2; margin-right: 2" onclick="addCouponModalShow()" >
                            {{-- <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user" alt="Delete icon"/> --}}
                            Add Coupon
                        </button>
                    </div>

                    <div class="card-body" style="text-align: right">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{-- Filter for pending}}

                        {{-- <table style="border:none; width:50%">
                            <tr class="nostyle" style="background-color: white; border:none;">
                                <td style="border-top: none">
                                    Class name :
                                </td>
                                <td style="border-top: none">
                                    <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="classSearchInput" onkeyup="filterStudent()" placeholder="Filter by class name.." title="Type in a class name">
                                </td>
                            </tr>
                        </table> --}}
                        <form name="ApprovalForm" method="POST" action="{{ url('/approveTransactions') }}">
                        @csrf
                        <table style="border: 1px solid black; width:100%" id="classTable">
                            <tr>
                                <th style="font-weight: bold; width:10%; text-align:center">
                                    No
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Coupon Code
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Value (RM)
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Date
                                </th>
                                <th style="text-align: center; vertical-align:middle; width:10%">
                                    <input type="checkbox" id="superCheckBox" onchange="selectAll()">
                                </th>
                                
                            </tr>

                            @if($user->store != null)
                                @if($transactions->count() == 0)
                                    <tr>
                                        <td colspan="5" style="text-align: center;">
                                            No pending coupons yet!
                                        </td>
                                    </tr>
                                @else
                                    @php
                                        $counter = 1;
                                    @endphp

                                    @foreach($transactions as $transaction)
                                    <tr>
                                        <td class="tdNo" style="text-align:center">
                                            {{ $counter++ }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $transaction['coupon_id'] }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $transaction->coupon->couponValue }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $transaction->coupon->couponDate }}
                                        </td>
                                        <td style="text-align: center">
                                            <input id="def" type="checkbox" class="checkBoxSelect" name="id[]" value="{{ __($transaction['id'])}}" onchange="EnableSubmit(this)" >
                                        </td>
                                    </tr>
                                    @endforeach

                                    {{-- <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                        <td colspan="3" style="text-align:center">
                                            No class matched you filter...
                                        </td> 
                                    </tr>--}}
                                @endif
                            @else
                            <tr>
                                <td colspan="5" style="text-align: center;">
                                    No pending coupons yet!
                                </td>
                            </tr>
                            @endif
                        </table>
                            <input type = "hidden" value="{{$user['id']}}" name="user_id">
                            
                            @if(Auth::User()->role == 'Admin')
                                <div id="pagingDiv2" style="border:none;">
                                    <span id="pagingSpan2" style="font-size:"></span>
                                    <br>
                                    <input type="submit" value="Approve" class="button1" onclick="return confirm('Confirm approve?')" id="abc" disabled>
                                    &nbsp
                                    <input type="submit" value="Remove" class="buttonReject" formaction="{{ url('/rejectTransactions') }}" onclick="return confirm('Confirm reject?')" id="ghi" disabled></span>
                                </div>
                            @endif
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color: #16731a; color:white; font-size:20"><b>Approved Coupons</b></div>
                    <div class="card-body" style="text-align: right">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{-- Filter for pending}}

                        {{-- <table style="border:none; width:50%">
                            <tr class="nostyle" style="background-color: white; border:none;">
                                <td style="border-top: none">
                                    Class name :
                                </td>
                                <td style="border-top: none">
                                    <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="classSearchInput" onkeyup="filterStudent()" placeholder="Filter by class name.." title="Type in a class name">
                                </td>
                            </tr>
                        </table> --}}
                        <table style="border: 1px solid black; width:100%" id="classTable">
                            <tr>
                                <th style="font-weight: bold; width:10%; text-align:center">
                                    No
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Coupon Code
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Value (RM)
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Date
                                </th>
                            </tr>
                            @if($user->store !=null)
                                @if($transacs->count() == 0)
                                    <tr>
                                        <td colspan="4" style="text-align: center;">
                                            No approved coupons yet!
                                        </td>
                                    </tr>
                                @else
                                    @php
                                        $counter = 1;
                                    @endphp
                                    @foreach($transacs as $trn)
                                    <tr>
                                        <td class="tdNo" style="text-align:center">
                                            {{ $counter++ }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $trn['coupon_id'] }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $trn->coupon->couponValue }}
                                        </td>
                                        <td style="text-align: center">
                                            {{ $trn->coupon->couponDate }}
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            @else
                                <tr>
                                    <td colspan="4" style="text-align: center;">
                                        No approved coupons yet!
                                    </td>
                                </tr>
                            @endif
                        </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if($user['role'] == "Student")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color: #16731a; color:white; font-size:20">
                        <b>Available Coupons</b>
                    </div>

                    <div class="card-body" style="text-align: right">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{-- Filter for pending}}

                        {{-- <table style="border:none; width:50%">
                            <tr class="nostyle" style="background-color: white; border:none;">
                                <td style="border-top: none">
                                    Class name :
                                </td>
                                <td style="border-top: none">
                                    <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="classSearchInput" onkeyup="filterStudent()" placeholder="Filter by class name.." title="Type in a class name">
                                </td>
                            </tr>
                        </table> --}}
                        <table style="border: 1px solid black; width:100%" id="">
                            <tr>
                                <th style="font-weight: bold; width:10%; text-align:center">
                                    No
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Coupon ID
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Value (RM)
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Date
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Scan
                                </th>
                            </tr>
                            @if($coupons->count() == 0)
                                <tr>
                                    <td colspan="5" style="text-align: center;">
                                        No available coupons yet!
                                    </td>
                                </tr>
                            @else
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach($coupons as $coupon)
                                
                                <tr>
                                    <td class="" style="text-align:center">
                                        {{ __($counter++)}}

                                    </td>
                                    <td style="text-align: center">
                                        {{ __($coupon->id)}}
                                    </td>
                                    <td style="text-align: center">
                                        RM {{ __($coupon->couponValue)}}
                                    </td>
                                    <td style="text-align: center">
                                        {{ __($coupon->couponDate)}}
                                    </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-primary" onclick="scanQRModalShow({{$coupon->id}})" >
                                                {{-- <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user" alt="Delete icon"/> --}}
                                                Scan
                                            </button>
                                            
                                        </center>
                                    </td>
                                </tr>
                                @endforeach
                                {{-- <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                    <td colspan="3" style="text-align:center">
                                        No class matched you filter...
                                    </td>
                                </tr>--}}
                            @endif
                        </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="background-color: #16731a; color:white; font-size:20"><b>Transaction History</b></div>
                    <div class="card-body" style="text-align: right">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif



                        <table style="border:none; width:50%">
                            <tr class="nostyle" style="background-color: white; border:none;">
                                <td style="border-top: none">
                                    Date :
                                </td>
                                <td style="border-top: none">
                                    <input class="form-control" style="border-radius: 6px; padding-left:10px" type="date" id="classSearchInput" onkeyup="" placeholder="Filter by date.." title="Type in a class name">
                                </td>
                            </tr>
                        </table>
                        <table style="border: 1px solid black; width:100%" id="classTable">
                            <tr>
                                <th style="font-weight: bold; width:10%; text-align:center">
                                    No
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Coupon ID
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Value (RM)
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Date
                                </th>
                                <th style="font-weight: bold; text-align: center; width:10%">
                                    Store
                                </th>
                            </tr>
                            @if($historys->count() == 0)
                                <tr>
                                    <td colspan="5" style="text-align: center;">
                                        No history coupons yet!
                                    </td>
                                </tr>
                            @else
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach($historys as $hist)
                                <tr>
                                    <td class="tdNo" style="text-align:center">
                                        {{ $counter++ }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ $hist->coupon->id }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ $hist->coupon->couponValue }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ $hist->coupon->couponDate }}
                                    </td>
                                    <td style="text-align: center">
                                        {{ $hist->store->storeName }}
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

{{-- POP UP BLOCK --}}

<div class="modal" id="editModal" style="border: solid">
    <div class="row justify-content-center" id="editModal2" >
        <div class="col-md-8 row justify-content-center" id="editModal3" style="border: none; text-align:center" >
            <div class="card" id="editCard" style="border: none; width: 60%">
                <div class="card-header" style="background-color: #16731a; color:white"><b>{{ __('Edit Profile') }}</b></div>

                <div class="card-body">
                    <form method="POST" action="{{ url('/editUser') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="userID" value="{{ __($user['id'])}}">

                        <div class="form-group row">
                            <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                            <div class="col-md-6 justify-content-center" >
                                <input type="file" id="real-input" name="fileToUpload">
                                    <button class="browse-btn" style="display: none">
                                        Browse Files
                                    </button>
                                    <span class="file-info" style="display: none">Upload a file</span>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @php
                                    $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                @endphp
                                @if(file_exists($ppLink))
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                @else
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> {{-- add / the begining to work. weird.... --}}
                                @endif
                                <span id="invalidFileExt" class="invalid-feedback-man" role="alert" style="">
                                    <strong>Please select .JPG or .PNG file for Profile Picture</strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') == "" ? $user['name'] : old('name') }}" placeholder="{{ old('email') == "" ? $user['name'] : old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') == "" ? $user['email'] : old('email') }}" placeholder="{{ old('email') == "" ? $user['email'] : old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Phone Number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                            <div class="col-md-6">
                                <input type="tel" id="userPhone" name="userPhone" class="form-control"  value="{{ old('phone_number') == "" ? $user['phone_number'] : old('phone_number') }}" placeholder="{{ old('phone_number') == "" ? $user['phone_number'] : old('phone_number') }}" pattern="[0-9]{10}" required>
                                @error('userPhone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="userRole" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                            <div class="col-md-6">
                                @if(Auth::user()->role == "Admin")
                                    <select name="userRole" class="form-control" required>
                                        <option class="form-control" selected hidden value="{{ old('role') == "" ? $user['role'] : old('role') }}">{{ old('role') == "" ? $user['role'] : old('role') }}</option>
                                        <option class="form-control" value="Admin">Admin</option>
                                        <option class="form-control" value="Store">Store</option>
                                        <option class="form-control" value="Student">Student</option>
                                    </select>
                                @else
                                    <input type="hidden" name="userRole" value="{{ __($user['role'])}}">
                                    <select name="" class="form-control" disabled required>
                                        <option class="form-control" selected hidden value="{{ old('role') == "" ? $user['role'] : old('role') }}">{{ old('role') == "" ? $user['role'] : old('role') }}</option>
                                    </select>
                                @endif
                            </div>
                        </div>

                        @if($user->role!= 'Student')
                            <div class="form-group row">
                                <label for="Store Name" class="col-md-4 col-form-label text-md-right">{{ __('Store Name') }}</label>

                                <div class="col-md-6">
                                    <input id="storeName" type="text" class="form-control @error('storeName') is-invalid @enderror" name="storeName" value="{{ old('storeName') != null ? old('storeName') : ($user->store == null ? '' : $user->store->storeName )}}" placeholder="{{ old('storeName') != null ? old('storeName') : ($user->store == null ? '' : $user->store->storeName) }}" required autocomplete="storeName" autofocus>

                                    @error('storeName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Store Address" class="col-md-4 col-form-label text-md-right">{{ __('Store Address') }}</label>

                                <div class="col-md-6">
                                    <input id="storeAddress" type="text" class="form-control @error('storeAddress') is-invalid @enderror" name="storeAddress" value="{{ old('storeAddress') != null ? old('storeAddress') : ($user->store == null ? '' : $user->store->storeAddress )}}" placeholder="{{ old('storeAddress') != null ? old('storeAddress') : ($user->store == null ? '' : $user->store->storeAddress )}}" required autocomplete="storeAddress" autofocus>

                                    @error('storeAddress')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif


                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                            <button type="submit" class="button1" style="width: 100">
                                {{ __('Submit') }}
                            </button>&nbsp &nbsp
                            <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                {{ __('Cancel') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="scanQRModal" style="border: solid">
    <div class="row justify-content-center" id="scanQRModal2" >
        <div class="col-md-8 row justify-content-center" id="scanQRModal3" style="border: none; text-align:center" >
            <div class="card" id="editCard" style="border: none; width: 60%">
                <div class="card-header" style="background-color: #16731a; color:white"><b>{{ __('Create QR') }}</b></div>

                <div class="card-body">

                    {{-- QR SHIT --}}
                    <input type="hidden" id='text' value="">

                    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                    <script type="text/javascript">
                        function generateBarCode()
                        {
                            var nric = $('#text').val();
                            var url = 'https://api.qrserver.com/v1/create-qr-code/?data=' + nric + '&amp;size=500x500';
                            $('#barcode').attr('src', url);
                        }
                    </script>

                    <img id='barcode' 
                        src="https://api.qrserver.com/v1/create-qr-code/?data=abdef&amp;size=200x200" 
                        title="Scan me!" 
                        width="250" 
                        height="250" />
                    <br><br>
                    <form method="POST" action="{{ url('/editUser') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="userID" value="{{ __($user['id'])}}">

                        {{-- DIV utk QR terus --}}

                        {{-- {!! QrCode::generate(Auth::user()->role.' ID:'.Auth::user()->id.' Phone No:'.Auth::user()->phone_number); !!} --}}

                        {{-- <div class="form-group row">
                            <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                            <div class="col-md-6 justify-content-center" >
                                <input type="file" id="real-input" name="fileToUpload">
                                    <button class="browse-btn" style="display: none">
                                        Browse Files
                                    </button>
                                    <span class="file-info" style="display: none">Upload a file</span>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @php
                                    $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                @endphp
                                @if(file_exists($ppLink))
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                {{-- @else
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> {{-- add / the begining to work. weird.... --}}
                                {{-- @endif
                                <span id="invalidFileExt" class="invalid-feedback-man" role="alert" style="">
                                    <strong>Please select .JPG or .PNG file for Profile Picture</strong>
                                </span>
                            </div>  
                        </div> --}}
                        
                        <div class="form-group row">
                            <label for="couponID" class="col-md-4 col-form-label text-md-right">{{ __('Coupon ID') }}</label>
                            <div class="col-md-6">
                                <input id="couponID" type="text" class="" name="couponID" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="couponValue" class="col-md-4 col-form-label text-md-right">{{ __('Coupon Value(RM)') }}</label>
                            <div class="col-md-6">
                                <input id="couponValue" type="text" class="" name="couponValue" disabled>                            
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="couponDate" class="col-md-4 col-form-label text-md-right">{{ __('Coupon Date') }}</label>
                            <div class="col-md-6">
                                <input id="couponDate" type="text" class="" name="couponDate" disabled>
                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                            <button type="submit" class="button1" style="width: 100">
                                {{ __('Submit') }}
                            </button>&nbsp &nbsp
                            <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                {{ __('Cancel') }}
                            </button>
                        </div>
                        &nbsp
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="addCouponModal" style="border: solid">
    <div class="row justify-content-center" id="addCouponModal2" >
        <div class="col-md-8 row justify-content-center" id="addCouponModal3" style="border: none; text-align:center" >
            <div class="card" id="editCard" style="border: none; width: 60%">
                <div class="card-header" style="background-color: #16731a; color:white"><b>{{ __('Add Coupon') }}</b></div>

                <div class="card-body">
                <form method="POST" action="{{url('/saveCouponManual') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="userID" value="{{ __($user['id'])}}">

                        {{-- <div class="form-group row">
                            <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                            <div class="col-md-6 justify-content-center" >
                                <input type="file" id="real-input" name="fileToUpload">
                                    <button class="browse-btn" style="display: none">
                                        Browse Files
                                    </button>
                                    <span class="file-info" style="display: none">Upload a file</span>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @php
                                    $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                @endphp
                                @if(file_exists($ppLink))
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                {{-- @else
                                    <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> {{-- add / the begining to work. weird.... --}}
                                {{-- @endif 
                                <span id="invalidFileExt" class="invalid-feedback-man" role="alert" style="">
                                    <strong>Please select .JPG or .PNG file for Profile Picture</strong>
                                </span> 
                            </div>
                        </div> --}}

                        <div class="form-group row">
                            <label for="couponID" class="col-md-4 col-form-label text-md-right">{{ __('Coupon ID') }}</label>
                            <div class="col-md-6">
                                <input id="couponID" type="text" class="form-control @error('couponID') is-invalid @enderror" name="couponID" value="" placeholder="Enter Coupon ID" required autocomplete="couponID" autofocus>

                                @error('couponID')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                            <button type="submit" class="button1" style="width: 100">
                                {{ __('Submit') }}
                            </button>&nbsp &nbsp
                            <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                {{ __('Cancel') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@if(session()->get('editProfileSuccess') != null)
<span id="editProfileSuccess" style="display: none"></span>
@endif
@if(session()->get('addedCoupon') != null)
<span id="addedCoupon" style="display: none">{{ session()->get('addedCoupon') }}</span>
<script>
    alert(document.getElementById("addedCoupon").innerText);
</script>
@endif
{{-- @if(session()->get('editProfileSuccess') != null)
<span id="editProfileSuccess" style="display: none"></span>
@endif --}}
{{-- @if(session()->get('editProfileSuccess') != null)
<span id="editProfileSuccess" style="display: none"></span>
@endif --}}



<script>
    window.onload=function(){

        // if(document.getElementById('addedCoupon') != null){
        //    alert(document.getElementById("addedCoupon").innerText);
        // }
    }

    var editModal = document.getElementById("editModal");
    var editModal2 = document.getElementById("editModal2");
    var editModal3 = document.getElementById("editModal3");

    var scanQRModal = document.getElementById("scanQRModal");
    var scanQRModal2 = document.getElementById("scanQRModal2");
    var scanQRModal3 = document.getElementById("scanQRModal3");

    var addCouponModal = document.getElementById("addCouponModal");
    var addCouponModal2 = document.getElementById("addCouponModal2");
    var addCouponModal3 = document.getElementById("addCouponModal3");

    // var changePasswordModal = document.getElementById("changePasswordModal");
    // var changePasswordModal2 = document.getElementById("changePasswordModal2");
    // var changePasswordModal3 = document.getElementById("changePasswordModal3");

    // var topupModal = document.getElementById("topupModal");
    // var topupModal2 = document.getElementById("topupModal2");
    // var topupModal3 = document.getElementById("topupModal3");

    function editModalShow() {
        editModal.style.display = "block";
        editModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
    };

    function scanQRModalShow(couponID) {
        scanQRModal.style.display = "block";
        scanQRModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
        

        var couponAidee = document.getElementById("couponID");
        var couponValue = document.getElementById("couponValue");
        var couponDate = document.getElementById("couponDate");

        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("/ajaxgetCouponDetails")}}',
                async: false,
                dataType: 'JSON',
                
                data: {
                    couponID : couponID,
                },
                success: function (data) {
                    
                    couponAidee.value = data.coupon.id;
                    couponAidee.placeholder = data.coupon.id;

                    couponValue.value = data.coupon.couponValue;
                    couponValue.placeholder = data.coupon.couponValue;

                    couponDate.value = data.coupon.couponDate;
                    couponDate.placeholder = data.coupon.couponDate;

                    document.getElementById("text").value = "tcocos.ddns.net/fyp/public/saveCoupon/"+data.coupon.id;
                    generateBarCode();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    
                },
            });
    };

    
    function addCouponModalShow() {
        addCouponModal.style.display = "block";
        addCouponModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
    };

    function selectAll(){
        var box = document.getElementsByClassName("checkBoxSelect");
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");
        if(document.getElementById("superCheckBox").checked == true){
            for(i = 0; i<box.length; i++ ){
                // box[i].checked = true;

                if(box[i].parentElement.parentElement.style.display==""){
                    box[i].checked = true;
                }
                else{
                    box[i].checked = false;
                }   
            }
            sbmt.disabled = false;
            sbmt1.disabled = false;
        }else{
            deselectAll();
        }
    }

    function deselectAll(){
        var superCheckBox = document.getElementById("superCheckBox");
        superCheckBox.checked = false;
        var box = document.getElementsByClassName("checkBoxSelect");
        for(i = 0; i<box.length; i++ ){
            box[i].checked = false;
        }
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");
        sbmt.disabled = true;
        sbmt1.disabled = true;
    }

    EnableSubmit = function(val)
    {
        var counter = 0;
        var counter1 = 0;
        var sbmt = document.getElementById("abc");
        var sbmt1 = document.getElementById("ghi");

        var superCheckBox = document.getElementById('superCheckBox');
        var checkBoxSelect = document.getElementsByClassName('checkBoxSelect');

        for(i = 0; i < checkBoxSelect.length; i++){
            if(checkBoxSelect[i].parentElement.parentElement.style.display=="" && checkBoxSelect[i].checked == true){
                counter++;
            }
        }
        if(counter>0){
            sbmt.disabled = false;
            sbmt1.disabled = false;
        }else{
            sbmt.disabled = true;
            sbmt1.disabled = true;
            superCheckBox.checked = false;
        }
        // alert(counter);
    };

    document.addEventListener('keydown', function(event){
        if(event.key === "Escape"){
            eventCloseEditFunction();
        }
    });

    document.addEventListener("click", function(e)
    {
        if (e.target==editModal || e.target==editModal2 || e.target==editModal3  ||
        e.target==scanQRModal || e.target==scanQRModal2 || e.target==scanQRModal3 ||
        e.target==addCouponModal || e.target==addCouponModal2 || e.target==addCouponModal3)
        {
            eventCloseEditFunction();
        }
    });

    function eventCloseEditFunction() {
        const body = document.body;
        body.style.overflowY = '';
        editModal.style.display = "none";
        changePasswordModal.style.display = "none";
        scanQRModal.style.display = "none";
        addCouponModal.style.display = "none";
    };
</script>

@endsection
@endguest
