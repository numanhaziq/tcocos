<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\User;
use App\Models\Coupon;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $students = User::where('role', '=', 'Student')->where('userStatus', '=', 'Active')->get();
            
            $currentDate = date('Y-m-d', strtotime(Carbon::now()));

            foreach($students as $student){
                //To create 2 RM5 coupons   
                for($i = 0; $i < 2; $i++){

                    $coupon = Coupon::create([
                        'couponValue' => 5,
                        'couponDate' => $currentDate,
                    ]);
                    
                    $coupon->user()->associate($student->id);
                    $coupon->save();
                    info($coupon->couponValue);
                }
                //To create 1 RM2 coupons 
                for($i = 0; $i < 1; $i++){

                    $coupon = Coupon::create([
                        'couponValue' => 2,
                        'couponDate' => $currentDate,
                    ]);
                    
                    $coupon->user()->associate($student->id);
                    $coupon->save();
                    info($coupon->couponValue);
                }
                //To create 3 RM1 coupons 
                for($i = 0; $i < 3; $i++){

                    $coupon = Coupon::create([
                        'couponValue' => 1,
                        'couponDate' => $currentDate,
                    ]);
                    
                    $coupon->user()->associate($student->id);
                    $coupon->save();
                    info($coupon->couponValue);
                }

            }
        })-> everyMinute();                                                                                      //ni for testing
        //weekdays()->between('10:00', '22:00'); ->generate coupon on weekdays starting at 10am (contoh)         //ni for real life situation
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
