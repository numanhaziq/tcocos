<?php

namespace App\Http\Controllers;
use App\Models\Coupon;
use App\Models\Transaction;
use App\Models\Store;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CouponController extends Controller
{
    
    //DISPLAY COUPON DETAILS INTO POP UP

    protected function displayCoupon(Request $request){
        
        info('coupon '. $request['couponID']);
        $coupon = Coupon::find($request->couponID);

        return response()->json(['coupon'=> $coupon]);
    }

    //SAVE COUPON INTO DATABASE

    protected function saveCoupon(Request $request){

        if(Auth::User() == null){
            return view('loginCoupon');
        }

        if(Auth::User()->role == "Store")
        {
            $transaction = Transaction::where('coupon_id', $request['couponID'])->get();
            $store = Store::where('user_id', Auth::User()->id)->first();
            $coupon = Coupon::find( $request['couponID']);

            info($store);
            info($coupon);

            if($coupon == null) //checks if coupon exist from database
            {
                return redirect('userProfile/'.Auth::User()->id)->with(['addedCoupon'=> 'Coupon does not exist!']);
            }

            //checks if couponDate is same to current date of the day
            if(date('Y-m-d', strtotime($coupon->couponDate)) < date('Y-m-d', strtotime(Carbon::now()))) //date('Y-m-d', strtotime(Carbon::now()))
            {
                return redirect('userProfile/'.Auth::User()->id)->with(['addedCoupon'=> 'Coupon have expired!']);
            }

            //checks if coupon scanned has been redeemed
            if($transaction->count() < 1)
            {
                $newTransaction = Transaction::create([
                    'transactionStatus' => "Pending",   
                ]);
                $newTransaction->coupon()->associate($request['couponID']);
                $newTransaction->store()->associate($store->id);
                $newTransaction->save();

                return redirect('userProfile/'.Auth::User()->id)->with(['addedCoupon'=> 'Coupon added!']);
            }

            return redirect('userProfile/'.Auth::User()->id)->with(['addedCoupon'=> "Coupon already redeemed!"]);
        }
        
        return redirect('userProfile/'.Auth::User()->id)->with(['addedCoupon'=> "You are not an authorized user!"]);
        
    }

    //SAVE COUPON BY MANUAL ENTRY

    protected function saveCouponManual(Request $request)
    {
        return redirect('/saveCoupon/'.$request['couponID']);
    }

    //APPPROVE COUPON

    protected function approveTransactions(Request $request)
    {
        $aidee = $request['user_id'];
        $ids = $request->input('id');
        $approvedUsersCounter = 0;
		foreach($ids as $transacID){
            $transac = Transaction::find($transacID);
            $transac['transactionStatus'] = 'Approved';
			$transac->save();
            $approvedUsersCounter++;
        }
        return redirect('/userProfile/'.$aidee)->with(['approvedUsersCounter'=> $approvedUsersCounter]);
    }

    //REJECT COUPON
    
    protected function rejectTransactions(Request $request)
    {
        $aidee = $request['user_id'];
        $ids = $request->input('id');
        $rejectedUsersCounter = 0;

		foreach($ids as $transacID){
            $transac = Transaction::find($transacID);
            $transac['transactionStatus'] = 'Rejected';
			$transac->delete();
            $rejectedUsersCounter++;
        }
        return redirect('/userProfile/'.$aidee)->with(['rejectedUsersCounter'=> $rejectedUsersCounter]);
    }
}
