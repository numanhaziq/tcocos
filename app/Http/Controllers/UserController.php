<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Coupon;
use App\Models\Store;
use App\Models\Transaction;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class UserController extends Controller
{
    //DISPLAY ALL USERS

    public function index()
    {
        
        $users = User::all()->toArray();
        // $stores = Store::all()->toArray();
        return view('home', compact('users'));

    }


    // VIEW SPECIFIC USER PROFILE

    protected function userProfile(Request $request)
    {
        $id = $request['id'];

        $user = User::find($id);
        if($user!=null){
            $firstName = explode(" ", $user['name']);
        }else{
            return redirect('userProfile/'.Auth::user()->id);
        }

        //TO TAKE COUPON FROM COUPON TABLE
        $coupons1 = Coupon::where('user_id', $id)->where('couponDate', date('Y-m-d', strtotime(Carbon::now())))->get();
        $coupons = collect(); //collection of object 
        $historys = collect();

        foreach($coupons1 as $coupon)
        {
            $transaction = Transaction::where('coupon_id', $coupon->id)->get();
            if($transaction->count() < 1)
            {
                $coupons->push($coupon);
                info($coupon);
            }
        }

        $transaction = null;

        //FOR STORE VIEW 
        if($user->role == 'Store')
        {
            if($user->store != null)
            {
                $transactions = Transaction::where('store_id', $user->store->id)->where('transactionStatus','Pending')->get();
                $transacs = Transaction::where('store_id', $user->store->id)->where('transactionStatus','Approved')->get();
    
                return view('/userprofile', compact('user', 'firstName', 'coupons', 'transactions', 'transacs'));
            }
            
            
        }

        //FOR STUDENT VIEW
        elseif($user->role == 'Student') //tanya nanti
        {
            $coupons2 = Coupon::where('user_id', $id)->get();
            info($coupons2->count());
            foreach($coupons2 as $coupon2)
            {
                //TO TAKE HISTORY TRANSACTION FOR USER
                $historys1 = Transaction::where('coupon_id', $coupon2->id)->get()->first();
                if($historys1 != null)
                {
                    info($historys1);
                    $historys -> push($historys1);
                }
                
            }
            info($historys->count());
            
            return view('/userprofile', compact('user', 'firstName', 'coupons', 'historys'));
        }

        return view('/userprofile', compact('user', 'firstName', 'coupons'));
        info($user->store);
    }

    // ADD USER

    public function adminRegister(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->create($request->all());

        $lastUser = User::orderby('id', 'DESC')->get()->first();
        $addedUser = $lastUser['name'];

        return redirect('/home')->with(['addedUser'=> $addedUser]);

    }

    protected function validator(array $data){
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['userPhone'],
            'userStatus' => 1,
            'role' => $data['userRole'],
            'password' => Hash::make($data['password']),
        ]);
    }

    //TO STORE(SIMPAN)???
    
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'email'=>'required'
        ]);

        $user = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number'),
            'role' => $request->get('role')
        ]);
        $user->save();
        return redirect('/home')->with('success', 'User saved!');

    }

    // EDIT USER

    public function editUser(Request $request)
    {
        $id = $request['userID'];
        $user = User::find($id);

        if($user['email'] != $request['email']){
            $this->validatorEdit($request->all())->validate();
        }
        
        //kiri is table database, kanan is from form

        $user['name'] = $request['name'];
        $user['email'] = $request['email'];
        $user['phone_number'] = $request['userPhone'];
        $user['role'] = $request['userRole'];

        if($user->store != null)
        {
            $store = Store::find($user->store->id);

            $store['storeName'] = $request['storeName'];
            $store['storeAddress'] = $request['storeAddress'];

            $store->save();
        }
        else
        {
            $store = Store::create([
                'storeName'=> $request['storeName'],
                'storeAddress' => $request['storeAddress'],
            ]);

            $store->user()->associate($user);
            $store->save();
        }

        $document = $request->file('fileToUpload');
        if($document != null){
            $document->getRealPath();
            $document->getClientOriginalName();
            $document->getClientOriginalExtension();
            $document->getSize();
            $document->getMimeType();
            $fileName = $id.".jpg";
            $destinationPath = "images/profilepictures";
            
            $document->move($destinationPath, $fileName);
        }
        $user->save();
        

        $editProfileSuccess = true;

        return redirect('userProfile/'.$user['id'])->with(['editProfileSuccess' => $editProfileSuccess]);
    }

    // DELETE SPECIFIC USER

    protected function deleteUser(Request $request){
        $id = $request['userID'];
        $user = User::find($id);
        $deletedUser = $user;
        $user->delete();

        return redirect('/home')->with(['deletedUser'=> $deletedUser]);
    }

    //CHANGE PASSWORD

    protected function changePassword(Request $request){
        $this->validatorCurrentPassword($request->all());
        $this->validatorChangePassword($request->all())->validate();

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->newPassword)]);

        if(Auth::User()->role != 'Admin')
        {
            return redirect('userProfile/'.auth()->user()->id)->with(['changePasswordSuccess'=>"Password changed successfully!"]);
        }
        return redirect('home')->with(['changePasswordSuccess'=>"Password changed successfully!"]);
        
    }

    protected function validatorCurrentPassword(array $data){
        if(!Hash::check($data['currentPassword'], auth()->user()->password)){
            throw ValidationException::withMessages(['currentPassword' => 'Incorrect current password']);
        }
    }

    protected function validatorChangePassword(array $data){
        return Validator::make($data, [
            'currentPassword' => ['required', 'string', 'min:8', ''],
            'newPassword' => ['required', 'string', 'min:8'],
            'confirm_newPassword' => ['required', 'same:newPassword'],
        ]);
    }

}
