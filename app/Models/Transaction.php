<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'transactionStatus',

    ];

    public function coupon(){

        return $this->belongsTo(Coupon::class);
        
    }

    public function store(){

        return $this->belongsTo(Store::class);
        
    }

}
