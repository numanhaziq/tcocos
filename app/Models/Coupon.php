<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'couponValue',
        'couponDate',

    ];

    public function user(){

        return $this->belongsTo(User::class);
        
    }

}

