<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Store extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'storeName',
        'storeAddress',

    ];

    public function user(){

        return $this->belongsTo(User::class);
        
    }
}
