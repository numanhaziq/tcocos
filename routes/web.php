<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\QrCodeGeneratorController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index/logout', function () {
    return view('logout');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/userProfile/{id}', [App\Http\Controllers\UserController::class, 'userProfile'])->name('userProfile'); //pegi specific user profile
Route::post('/editUser', [App\Http\Controllers\UserController::class, 'editUser'])->name('editUser'); //edit user profile
Route::post('/adminRegister', [App\Http\Controllers\UserController::class, 'adminRegister'])->name('adminRegister');  //add user
Route::post('/deleteUser', [App\Http\Controllers\UserController::class, 'deleteUser'])->name('deleteUser'); //delete user
Route::post('/changePassword', [App\Http\Controllers\UserController::class, 'changePassword'])->name('changePassword'); //change password

Route::any('/ajaxgetCouponDetails', [App\Http\Controllers\CouponController::class, 'displayCoupon'])->name('displayCoupon'); //ajax to bring coupon deets into pop up
Route::any('/saveCoupon/{couponID}', [App\Http\Controllers\CouponController::class, 'saveCoupon'])->name('saveCoupon'); //scan QR to save coupon into database
Route::any('/saveCouponManual', [App\Http\Controllers\CouponController::class, 'saveCouponManual'])->name('saveCouponManual'); //manual entry to save coupon into database
Route::post('/approveTransactions', [App\Http\Controllers\CouponController::class, 'approveTransactions'])->name('approveTransactions'); //approve Pending coupons
Route::post('/rejectTransactions', [App\Http\Controllers\CouponController::class, 'rejectTransactions'])->name('rejectTransactions'); //reject Pending coupons


// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('users','UserController');

//QR Code

Route::get('/qr-code', [QrCodeGeneratorController::class, 'index'])->name('qr.code.index');

// Route::get('qr-code-g', function () {
  
//     \QrCode::size(500)
//             ->format('png')
//             ->generate('ItSolutionStuff.com', public_path('images/qrcode.png'));
    
//   return view('qrCode');
    
// });

// Route::get('qrcode', function () {
//     return QrCode::size(300)->generate('A basic example of QR code!');
// });
